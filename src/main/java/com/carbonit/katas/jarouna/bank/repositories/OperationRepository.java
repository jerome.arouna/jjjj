package com.carbonit.katas.jarouna.bank.repositories;

import com.carbonit.katas.jarouna.bank.entities.Operation;

import java.util.List;

public interface OperationRepository {
    void add(Operation operation);

    List<Operation> findAll();
}
