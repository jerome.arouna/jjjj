package com.carbonit.katas.jarouna.bank.outputs;

public class ConsoleOutputInterface implements OutputInterface {
    @Override
    public void output(String message) {
        System.out.print(message);
    }
}
